/*

Exercício 5
Autor: Kessyus Fófano dos Santos

05 - Escreva um programa que percorra todos os valores de um array de objetos contendo como atributos o nome
e o sexo dos alunos e permita que o usuário informe, 1 se o usuário está presente ou 2 se o usuário faltou,
e após o usuário responder todos os itens o programa deverá exibir todos os itens da lista e o seu status de
presença. Observação: para cada item da lista respondido o valor informado pelo usuário deverá ser armazenado
para que possa estar sendo apresentado ao final do programa.

*/

var readlineSync = require('readline-sync');

var listaAlunos = [
                    {nome: "Kessyus", sexo: "masculino"}, 
                    {nome: "Júlia", sexo: "feminino"},
                    {nome: "Roberto", sexo: "masculino"},
                    {nome: "Camila", sexo: "feminino"},
                    {nome: "Felipe", sexo: "masculino"},
                    {nome: "Fernanda", sexo: "feminino"}
                ];

// cria um array do tamanho da lista de alunos com todos os itens igual a 0.
var presenca = new Array(listaAlunos.length).fill(0);

// roda a lista de alunos perguntando sobre a presenca de cada um:
for (let i = 0; i < listaAlunos.length; i++)
    presenca[i] = readlineSync.question('O aluno ' + listaAlunos[i].nome + ' do sexo ' + listaAlunos[i].sexo + ', está presente?\n1 - Sim, 2 - Não\n');

// imprime a lista de presença
console.log('\nLista de presença:\n');
for(let i = 0; i < listaAlunos.length; i++)
    console.log((i+1) + ' - ' + listaAlunos[i].nome + ' ' + listaAlunos[i].sexo + ' ' + ((presenca[i]==='1') ? 'Presente' : 'Faltou'));