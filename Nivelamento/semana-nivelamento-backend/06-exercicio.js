/*

Exercício 6
Autor: Kessyus Fófano dos Santos

06 - Escreva um programa que solicite ao usuário o primeiro nome e o programa imprima na tela
este nome 100 vezes, porém ao imprimir o nome solicitado o programa deverá exibir para cada linha
apresentada o nome 10 vezes. Exemplo

Informe o nome:  Ezer
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 1 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 2 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 3 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 4 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 5 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 6 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 7 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 8 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 9 
Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | Ezer | - linha 10
Fim do programa

*/

var readlineSync = require('readline-sync');

const nome = readlineSync.question('Informe o nome: ');

for (let i = 0; i < 10; i++) {
    var texto = "";
    for (let j = 0; j < 10; j++) {
        texto = texto + nome + " | ";
    }
    console.log(texto + '- linha ' + (i+1));
}
console.log('Fim do programa\n');