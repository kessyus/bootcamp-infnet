/*

Exercício 3
Autor: Kessyus Fófano dos Santos

03 - Escreva um programa que solicite ao usuário um número qualquer e o programa responda
imprimindo na tela se o número informado é um número primo.

*/

var readlineSync = require('readline-sync');
const numUsuario = readlineSync.question('Por favor informe um número.\n');

// boolean que afirma ou nega se o número é primo;
var numPrimo = true;

// checa se o número é menor ou igual a 1
if (numUsuario <= 1) 
    console.log('O número informado é menor ou igual a 1. Dessa forma ele não é primo.');
else if (numUsuario > 1) {

    for (let i = 2; i < numUsuario; i++) {
        if (numUsuario % i === 0) {
            numPrimo = false;
            break;
        }
    }

    // imprime se é primo ou não baseado no booleano numPrimo
    if (numPrimo)
        console.log('O número ' + numUsuario + ' é primo.');
    else
        console.log('O número ' + numUsuario + ' não é primo.');
}