/*

Exercício 1
Autor: Kessyus Fófano dos Santos

01 - Escreva um programa que solicite ao usuário que informe o tamanho dos três lados de um triângulo
e o programa retorno o tipo de triângulo correspondente aos valores informados. Observação:

Triângulo Equilátero 3 lados iguais
Triângulo Isósceles 2 lados iguais
Triângulo Escaleno todos os lados diferentes

*/

var readlineSync = require('readline-sync');

console.log('Por favor informe o tamanho dos lados do triângulo.\n');

const lado1 = readlineSync.question('Lado 1: \n');
const lado2 = readlineSync.question('Lado 2: \n');
const lado3 = readlineSync.question('Lado 3: \n');

if ( (lado1 === lado2) && (lado1 === lado3) ) {
    console.log('O triângulo é equilátero.\n');
    return;
}

if ( (lado1 !== lado2) && (lado1 !== lado3) && (lado2 !== lado3) ) {
    console.log('O triângulo é escaleno.\n');
    return;
}

if ( ((lado1 === lado2) && (lado1 !== lado3)) || ((lado1 !== lado2) && (lado1 === lado3)) || ((lado1 !== lado2) && (lado2 === lado3)) ) {
    console.log('O triângulo é isóceles.\n');
    return;
}