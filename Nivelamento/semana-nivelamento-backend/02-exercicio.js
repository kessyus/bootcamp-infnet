/*

Exercício 2
Autor: Kessyus Fófano dos Santos

02 - Escreva um programa que solicite ao usuário um número qualquer e o programa responda imprimindo
na tela se o número informado é impar ou par.

*/

var readlineSync = require('readline-sync');
const numUsuario = readlineSync.question('Por favor informe um número.\n');

if (numUsuario % 2 === 0)
    console.log('O número é par.\n');
else 
    console.log('O número é ímpar.\n');