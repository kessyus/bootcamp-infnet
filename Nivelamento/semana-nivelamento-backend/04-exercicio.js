/*

Exercício 4
Autor: Kessyus Fófano dos Santos

04 - Escreva um programa que apresente na tela do usuário uma lista de opções como apresentado abaixo:
1 - Somar | 2 - Subtrair | 3 - Multiplicar | 4 - Dividir | -1 Sair 
Após apresentar as opções ao usuário o programa deverá recolher do usuário a opção desejada:
Se o valor informado for igual a uma operação matemática, o programa deverá solicitar o primeiro número
ao usuário, na sequência deverá solicitar o segundo número e por fim apresentar ao usuário o resultado
final da operação. 

Se o valor informado for -1 que corresponde a opção Sair apresentada o sistema deverá apresentar uma
mensagem avisando que está desligando e o programa deve ser encerrado.
Observação: o programa deve permanecer ligado até que o usuário use a opção “-1 Sair”.

*/

var readlineSync = require('readline-sync');

// função para calcular a operação escolhida pelo usuário
function calcula(operacao) {
    var num1 = readlineSync.question('Informe o primeiro número:\n');
    var num2 = readlineSync.question('Informe o segundo número:\n');
    var resposta = "";
    switch (operacao) {
        case 1:     // 1 = somar
            resposta = num1 + ' + ' + num2 + ' = ' + (parseInt(num1) + parseInt(num2));
            break;
        case 2:     // 2 = subtrair
            resposta = num1 + ' - ' + num2 + ' = ' + (num1 - num2);
            break;
        case 3:     // 3 = multiplicar
            resposta = num1 + ' * ' + num2 + ' = ' + (num1 * num2);
            break;
        case 4:     // 4 = dividir
            resposta = num1 + ' / ' + num2 + ' = ' + (num1 / num2);
            break;
    }
    return resposta;
}

while (true) {

    var opcaoUsuario = readlineSync.question('1 - Somar | 2 - Subtrair | 3 - Multiplicar | 4 - Dividir | -1 Sair\n');

    switch (opcaoUsuario) {
        case '1':
            // executa a operação de somar -> 1
            console.log(calcula(1) + '\n\n');
            break;
        case '2':
            // executa a operação de subtrair -> 2
            console.log(calcula(2) + '\n\n');
            break;
        case '3':
            // executa a operação de multiplicar -> 3
            console.log(calcula(3) + '\n\n');
            break;
        case '4':
            // executa a operação de dividir -> 4
            console.log(calcula(4) + '\n\n');
            break;
        case '-1':
            // sai do programa
            console.log('Desligando o sistema...\n');
            return;
        default:
            console.log('A opção escolhida não está correta. Favor digitar novamente.\n\n');
            break;
    }

}